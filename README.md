# HackTheBox Write Ups - Jacob Scheetz #

This repository is to document the progress made on personal CTF's and boxes/rooms on both TryHackMe and HackTheBox. Write ups for specific boxes can be found in the boxes folder. This is meant to serve as a personal cheat sheet for useful commands, code snippets, reverse shells, worldlists, etc. 

* Any questions about these write ups can be emailed to: scheetzj2@udayton.edu
* Version 1.0


### Useful Exploit Databases ###
* https://www.exploit-db.com 
* https://www.rapid7.com/db/

## Cheat Sheets ##

### NCL :: Basic Linux Bash ###
* `cd` --> Stands for "change directory". This is how you navigate between directories, or "folders"
* `pwd` --> Stands for "print working directory". This prints the directory you are currently located in
* `ls` --> Think of this as an abbreviation for the word "list". This will list the contents of the directory you are located in
* `cat` --> used to print the contents of a file into 
* `nano` , `vim` --> used to edit files from the command line. Think of these as your word program of the linux terminal
* `su` --> stands for "switch user". allows you to change users on the system
* `sudo` --> backcronym is "SuperUser do" or "switch user do". This allows you to run commands as the root, highest level privileged account, on the system. 

### NCL :: file obastacles and hurdles ###
* hidden files begin with a dot and can be found with `ls -a` -->  i.e. `.bashrc`
* files named as a dash "-" will give us trouble trying to read the content because the program we try to read it in will think we are giving it an argument. To get around this we use redirection to get the contents. Redirection is using the ">" operator to send a file to a program. i.e. --> `- > cat` will print the contents of the "-" file with the cat program. 
* `file` --> gives us the header information about any type of file, which can be important if we are trying to figure out the contents, the information stored in the file, etc. 
* `find ./ -name filename` --> can search the system for a file and let us know where it is located
* `whereis` --> can also be used to find the path to an executable
* `uniq -c` can be used to count the number of occurences in a file
* `grep` --> can be used to search a file for a particular string. i.e. `grep  suspicious example-logfile.log`  
* `strings` --> basically "greps" an executable for a particular string


### Recon ###
* `nmap -Pn -A -sV - T4 target address` --> scans network on stealth mode, with all tickers, for service at a speed of 80%
* `wpscan -u http://webadress.com/` --enumerate t --enumerate p --enumerate u --> enumerates login credentials
* `wpscan -u http://webaddress.com/` --username 'foundusername' --wordlist /path/to/wordlist --> gets the password from a wordlist with a matching username
* `dirb http://webaddress/ /user/Desktop/filedest.txt` --> enumerates directories on website
* `steghide --info image.type` --> to find hidden files in an image
* `steghide --extract -sf image.type` --> extracts the image and leaves the hidden file, password is typically empty 

### Shell Access ###
* cd to home directory, find . -name 'file name' --> gives path to file 
* look at .bash_history, .bashrc, '/*sh_hisotry' --> has the history of typed commands into shell, usually contains important commands used 
* etc/passwd --> explanatory 
* `ls -ltr` --> lists the permisions of all the files in a directory 
* `ls -a` --> hidden files
* `chmod +x file` --> sets execute bit for binary or file
* MOST of the time trying to set up a reverse shell --> where we open up a port and get the user to connect via TCP to us
* `sudo -l` --> lists commands you can run as specific users 
* `ls -ltrh` --> gives the permissions of the files in directory 
* `sudo -u user awk 'BEGIN {system("/bin/bash")}'`  --> run a shell with awk to get access to the users shell you sudo as


