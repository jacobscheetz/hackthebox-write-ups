# Machine Write Up ::: Lame #
* OS: Linux, difficulty: easy, CVE, metasploitable box, metasploit framework, python
* htb-user: sxh, Jacob Scheetz

## Steps to Solve ##
* Disclaimer: the steps to solve this box are either my own, in conjunction with students in the ACM of UD, or a combination of both
* these steps are condensed to show the correct path to establishing a reverse shell for the target machine, this write up does NOT in any way reveal the flag or encourage flag sharing for the exercise


### step one: reconnaisance ###
* as always begin with a nmap scan: 
nmap scan here

* results indicate that there is multiple vulnerable services running on the box (vsftpd, samba, etc)

* `searchsploit` the vulnerable service to see if there are any CVE's that are documented on exploiting
searchsploit results here


### step two: setting up the exploit ###
* we now know from the searchsploit results that there is a vulnerable samba service running that can be found in msf console 
* to find this we run `msfconsole` then `search samba` 
* there are a lot of exploits that come up but we are looking for the one that matches what we found on searchsploit (user map)

* on this list of exploits there is a number next to each line, we use the command `use exploit <number line exploit was on>` - and now the payload is loaded 
* use `show options` to make sure the options are set like (RHOSTS, LHOSTS, etc) 


### step three: exploiting ###
* once the exploit is run we should have an open session on the machine
* the shell command can be used to get a bash session open via python 
* the command `pwd` shows we are in the root directory

